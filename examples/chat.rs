extern crate docopt;
extern crate futures;
extern crate peer_to_peer;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate tokio;
extern crate tokio_io;
extern crate tokio_timer;
extern crate uuid;

use std::{io, thread};
use std::time::Duration;

use docopt::Docopt;
use futures::{Future, Stream};
use futures::sync::oneshot::channel;
use tokio::executor::current_thread;
use tokio_timer::Timer;
use peer_to_peer::{tcp, Event, Peer};
use uuid::Uuid;

static USAGE: &'static str = "
Commands:
    server  Starts a server. Servers must be provided a unique address (ip:port)
            at startup, along with the ID and address of all peer servers.
Usage:
    chat [<peers>] ...
    chat (-h | --help)
Options:
    -h --help   Show a help message.
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_peers: Vec<String>,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let mut peers = args.arg_peers
        .iter()
        .map(|x| x.parse().unwrap())
        .collect::<Vec<_>>();

    let addr = peers.remove(0);
    let (peer, peer_receiver) = Peer::new(Uuid::new_v4(), addr);

    let (tcp_close, tcp_future) = tcp::tcp(&peer, peers).unwrap();

    let (peer_close_sender, peer_close_receiver) = channel();
    let peer_receiver_future = peer_receiver
        .for_each(|event| {
            match event {
                Event::Listen(id, addr) => println!("{:?} listening on {:?}", id, addr),

                Event::Connect(peer_id, peer_addr) => {
                    println!("peer connected {:?} {:?}", peer_id, peer_addr)
                }
                Event::Disconnect(peer_id, peer_addr) => {
                    println!("peer disconnected {:?} {:?}", peer_id, peer_addr)
                }

                Event::Payload(from, data) => println!("payload from {:?} {:?}", from, data),
                Event::Bytes(from, bytes) => println!("bytes from {:?} {:?}", from, bytes),

                Event::ConnectError(error) => println!("failed to connect {:?}", error),
                Event::Error(error) => println!("ERROR {:?}", error),
            }
            Ok(())
        })
        .select(peer_close_receiver.map(|_| ()).map_err(|_| ()))
        .map(|_| ())
        .map_err(|_| ());

    let gossip_peer = peer.clone();
    let (gossip_close_sender, gossip_close_receiver) = channel();
    let gossip_future = Timer::default()
        .interval(Duration::from_secs(1))
        .map_err(|e| println!("{:?}", e))
        .for_each(move |_| {
            let _ = gossip_peer.send_peers_random();
            Ok(())
        })
        .select(gossip_close_receiver.map(|_| ()).map_err(|_| ()))
        .map(|_| ())
        .map_err(|_| ());

    let input_peer = peer.clone();
    let input_handle = thread::spawn(move || loop {
        let mut input = String::new();

        match io::stdin().read_line(&mut input) {
            Ok(_) => {
                let _ = input.pop();

                if input == ":q" || input == ":quit" {
                    tcp_close.close();
                    let _ = peer_close_sender.send(());
                    let _ = gossip_close_sender.send(());
                    break;
                } else if input.starts_with("\\b") {
                    let _ = input_peer.broadcast_bytes(input.as_bytes().to_vec());
                } else {
                    let _ = input_peer.broadcast(input);
                }
            }
            Err(error) => println!("{}", error),
        }
    });

    current_thread::run(move |_| {
        current_thread::spawn(tcp_future);
        current_thread::spawn(peer_receiver_future);
        current_thread::spawn(gossip_future);
    });

    let _ = input_handle.join();
}
