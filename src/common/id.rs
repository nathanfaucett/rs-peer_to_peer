use std::hash::Hash;

use serde::ser::Serialize;
use serde::de::DeserializeOwned;

pub trait Id: 'static + Send + Serialize + DeserializeOwned + Clone + Eq + Hash {}

impl<T> Id for T
where
    T: 'static + Send + Serialize + DeserializeOwned + Clone + Eq + Hash,
{
}
