use std::io::Error;
use std::net::SocketAddr;

use super::{Id, Value};

#[derive(Debug)]
pub enum Event<ID, T>
where
    ID: Id,
    T: Value,
{
    Listen(ID, SocketAddr),

    Connect(ID, SocketAddr),
    Disconnect(ID, SocketAddr),

    Payload(ID, T),
    Bytes(ID, Vec<u8>),

    ConnectError(Error),
    Error(Error),
}

unsafe impl<T: Value, ID: Id> Send for Event<ID, T> {}
unsafe impl<T: Value, ID: Id> Sync for Event<ID, T> {}
