use std::net::SocketAddr;
use std::collections::HashSet;

use futures::Sink;
use futures::sync::mpsc::{unbounded, UnboundedReceiver, UnboundedSender};

use super::super::Peers;
use super::{Event, Id, Packet, Value};

pub struct Peer<ID, T>
where
    ID: Id,
    T: Value,
{
    sender: UnboundedSender<Event<ID, T>>,
    peers: Peers<ID>,
}

impl<ID, T> Clone for Peer<ID, T>
where
    ID: Id,
    T: Value,
{
    #[inline]
    fn clone(&self) -> Self {
        Peer {
            sender: self.sender.clone(),
            peers: self.peers.clone(),
        }
    }
}

unsafe impl<T: Value, ID: Id> Send for Peer<ID, T> {}
unsafe impl<T: Value, ID: Id> Sync for Peer<ID, T> {}

impl<ID, T> Peer<ID, T>
where
    ID: Id,
    T: Value,
{
    #[inline]
    pub fn new(id: ID, addr: SocketAddr) -> (Self, UnboundedReceiver<Event<ID, T>>) {
        let (tcp_sender, tcp_receiver) = unbounded();

        let tcp_peer = Peer {
            sender: tcp_sender,
            peers: Peers::new(id, addr),
        };

        (tcp_peer, tcp_receiver)
    }

    #[inline]
    pub fn id(&self) -> &ID {
        self.peers.id()
    }
    #[inline]
    pub fn addr(&self) -> &SocketAddr {
        self.peers.addr()
    }

    #[inline]
    pub fn close(&mut self) {
        let _ = self.sender.close();
    }

    #[inline]
    pub fn is_id_connected(&self, id: &ID) -> bool {
        self.peers.is_id_connected(id)
    }

    #[inline]
    pub fn connected_ids(&self) -> Vec<ID> {
        self.peers.connected_ids()
    }
    #[inline]
    pub fn connected_addrs(&self) -> Vec<SocketAddr> {
        self.peers.connected_addrs()
    }

    #[inline]
    pub fn connected_count(&self) -> u64 {
        self.peers.connected_count()
    }

    #[inline]
    pub fn send_bytes(&self, id: &ID, bytes: Vec<u8>) -> Result<(), Vec<u8>> {
        self.peers
            .send(id, &Packet::Bytes::<ID, T>(self.id().clone(), bytes))
    }
    #[inline]
    pub fn broadcast_bytes(&self, bytes: Vec<u8>) -> Result<(), (Vec<u8>, Vec<ID>)> {
        self.peers
            .broadcast(&Packet::Bytes::<ID, T>(self.id().clone(), bytes))
    }
    #[inline]
    pub fn broadcast_bytes_to<'a, I>(
        &self,
        ids: I,
        bytes: Vec<u8>,
    ) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        ID: 'a + Id,
        I: 'a + IntoIterator<Item = &'a ID>,
    {
        self.peers
            .broadcast_to(ids, &Packet::Bytes::<ID, T>(self.id().clone(), bytes))
    }
    #[inline]
    pub fn broadcast_bytes_except(
        &self,
        ids: &HashSet<ID>,
        bytes: Vec<u8>,
    ) -> Result<(), (Vec<u8>, Vec<ID>)> {
        self.peers
            .broadcast_except(ids, &Packet::Bytes::<ID, T>(self.id().clone(), bytes))
    }
    #[inline]
    pub fn send_bytes_random(&self, bytes: Vec<u8>) -> Result<(), Vec<u8>> {
        self.peers
            .send_random(&Packet::Bytes::<ID, T>(self.id().clone(), bytes))
    }

    #[inline]
    pub fn send(&self, id: &ID, data: T) -> Result<(), Vec<u8>> {
        self.peers
            .send(id, &Packet::Payload::<ID, T>(self.id().clone(), data))
    }
    #[inline]
    pub fn broadcast(&self, data: T) -> Result<(), (Vec<u8>, Vec<ID>)> {
        self.peers
            .broadcast(&Packet::Payload::<ID, T>(self.id().clone(), data))
    }
    #[inline]
    pub fn broadcast_to<'a, I>(&self, ids: I, data: T) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        ID: 'a + Id,
        I: 'a + IntoIterator<Item = &'a ID>,
    {
        self.peers
            .broadcast_to(ids, &Packet::Payload::<ID, T>(self.id().clone(), data))
    }
    #[inline]
    pub fn broadcast_except(&self, ids: &HashSet<ID>, data: T) -> Result<(), (Vec<u8>, Vec<ID>)> {
        self.peers
            .broadcast_except(ids, &Packet::Payload::<ID, T>(self.id().clone(), data))
    }
    #[inline]
    pub fn send_random(&self, data: T) -> Result<(), Vec<u8>> {
        self.peers
            .send_random(&Packet::Payload::<ID, T>(self.id().clone(), data))
    }

    #[inline]
    pub fn send_peers_random(&self) -> Result<(), Vec<u8>> {
        self.peers
            .send_random(&Packet::Peers::<ID, T>(self.peers.connected()))
    }

    #[inline]
    pub fn connect(
        &self,
        id: ID,
        peer_addr: SocketAddr,
        local_addr: SocketAddr,
        sender: UnboundedSender<Vec<u8>>,
    ) {
        self.peers
            .connect(id.clone(), peer_addr.clone(), local_addr, sender);
        self.send_event(Event::Connect(id, peer_addr));
    }
    #[inline]
    pub fn disconnect(&self, local_addr: &SocketAddr) -> Option<SocketAddr> {
        if let Some((id, peer_addr)) = self.peers.disconnect(local_addr) {
            self.send_event(Event::Disconnect(id, peer_addr));
            Some(peer_addr)
        } else {
            None
        }
    }

    #[inline]
    pub(crate) fn send_event(&self, event: Event<ID, T>) {
        self.sender
            .unbounded_send(event)
            .expect("failed to send tcp event");
    }
}
