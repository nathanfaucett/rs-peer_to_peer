use std::convert::TryInto;
use std::io::{Error, ErrorKind};
use std::net::SocketAddr;
use std::marker::PhantomData;

use bincode;
use bytes::{BufMut, Bytes, BytesMut};
use tokio_io::codec::{Decoder, Encoder};

use super::{Id, Value};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum Packet<ID, T> {
    Ping(ID, SocketAddr),
    Pong(ID, SocketAddr),
    Peers(Vec<(ID, SocketAddr)>),
    Payload(ID, T),
    Bytes(ID, Vec<u8>),
}

unsafe impl<T: Value, ID: Id> Send for Packet<ID, T> {}
unsafe impl<T: Value, ID: Id> Sync for Packet<ID, T> {}

impl<ID, T> TryInto<Bytes> for Packet<ID, T>
where
    ID: Id,
    T: Value,
{
    type Error = Error;

    #[inline]
    fn try_into(self) -> Result<Bytes, Self::Error> {
        match bincode::serialize(&self, bincode::Infinite) {
            Ok(bytes) => Ok(bytes.into()),
            Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
        }
    }
}
impl<ID, T> TryInto<Packet<ID, T>> for Bytes
where
    ID: Id,
    T: Value,
{
    type Error = Error;

    #[inline]
    fn try_into(self) -> Result<Packet<ID, T>, Self::Error> {
        match bincode::deserialize(&*self) {
            Ok(packet) => Ok(packet),
            Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
        }
    }
}

impl<ID, T> TryInto<Vec<u8>> for Packet<ID, T>
where
    ID: Id,
    T: Value,
{
    type Error = Error;

    #[inline]
    fn try_into(self) -> Result<Vec<u8>, Self::Error> {
        match bincode::serialize(&self, bincode::Infinite) {
            Ok(bytes) => Ok(bytes.into()),
            Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
        }
    }
}
impl<ID, T> TryInto<Packet<ID, T>> for Vec<u8>
where
    ID: Id,
    T: Value,
{
    type Error = Error;

    #[inline]
    fn try_into(self) -> Result<Packet<ID, T>, Self::Error> {
        match bincode::deserialize(&*self) {
            Ok(packet) => Ok(packet),
            Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PacketCodec<ID: Id, T>(PhantomData<(ID, T)>);

impl<ID, T> Default for PacketCodec<ID, T>
where
    ID: Id,
    T: Value,
{
    #[inline(always)]
    fn default() -> Self {
        PacketCodec(PhantomData)
    }
}

impl<ID, T> PacketCodec<ID, T>
where
    ID: Id,
    T: Value,
{
    #[inline(always)]
    pub fn new() -> Self {
        PacketCodec(PhantomData)
    }
}

impl<ID, T> Decoder for PacketCodec<ID, T>
where
    ID: Id,
    T: Value,
{
    type Item = Packet<ID, T>;
    type Error = Error;

    #[inline]
    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.is_empty() {
            Ok(None)
        } else {
            match bincode::deserialize(&*src.take()) {
                Ok(packet) => Ok(Some(packet)),
                Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
            }
        }
    }
}

impl<ID, T> Encoder for PacketCodec<ID, T>
where
    ID: Id,
    T: Value,
{
    type Item = Packet<ID, T>;
    type Error = Error;

    #[inline]
    fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
        match bincode::serialize(&item, bincode::Infinite) {
            Ok(bytes) => {
                dst.put(bytes);
                Ok(())
            }
            Err(e) => Err(Error::new(ErrorKind::Other, e.to_string())),
        }
    }
}
