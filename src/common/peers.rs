use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::net::SocketAddr;
use std::collections::hash_map;
use std::collections::{HashMap, HashSet};

use bincode;
use futures::sync::mpsc::UnboundedSender;
use rand::{thread_rng, Rng};
use serde::ser::Serialize;

use super::Id;

pub struct PeersInner<ID>
where
    ID: Id,
{
    id: ID,
    addr: SocketAddr,
    map: RwLock<PeersMap<ID>>,
}

pub struct Peers<ID: Id>(Arc<PeersInner<ID>>);

unsafe impl<ID: Id> Send for Peers<ID> {}
unsafe impl<ID: Id> Sync for Peers<ID> {}

impl<ID> Clone for Peers<ID>
where
    ID: Id,
{
    #[inline]
    fn clone(&self) -> Self {
        Peers(self.0.clone())
    }
}

impl<ID> Peers<ID>
where
    ID: Id,
{
    #[inline]
    pub fn new(id: ID, addr: SocketAddr) -> Self {
        Peers(Arc::new(PeersInner {
            id: id,
            addr: addr,
            map: RwLock::new(PeersMap::new()),
        }))
    }

    #[inline(always)]
    pub fn id(&self) -> &ID {
        &self.0.id
    }
    #[inline(always)]
    pub fn addr(&self) -> &SocketAddr {
        &self.0.addr
    }

    #[inline]
    pub fn map(&self) -> RwLockReadGuard<PeersMap<ID>> {
        self.0
            .map
            .read()
            .expect("failed to acquire peers read lock")
    }
    #[inline]
    pub fn map_mut(&self) -> RwLockWriteGuard<PeersMap<ID>> {
        self.0
            .map
            .write()
            .expect("failed to acquire peers write lock")
    }

    #[inline]
    pub fn is_id_connected(&self, id: &ID) -> bool {
        self.id() == id || self.map().senders.contains_key(id)
    }

    #[inline]
    pub fn connect(
        &self,
        id: ID,
        peer_addr: SocketAddr,
        local_addr: SocketAddr,
        sender: UnboundedSender<Vec<u8>>,
    ) {
        let mut map_mut = self.map_mut();

        map_mut.local_addrs.insert(local_addr, id.clone());
        map_mut.senders.insert(id, (peer_addr, sender));
    }
    #[inline]
    pub fn disconnect(&self, local_addr: &SocketAddr) -> Option<(ID, SocketAddr)> {
        let mut map_mut = self.map_mut();

        if let Some(id) = map_mut.local_addrs.remove(local_addr) {
            if let Some((peer_addr, _)) = map_mut.senders.remove(&id) {
                Some((id, peer_addr))
            } else {
                None
            }
        } else {
            None
        }
    }

    #[inline]
    pub fn connected(&self) -> Vec<(ID, SocketAddr)> {
        let map = self.map();
        map.senders
            .iter()
            .map(|(id, &(ref addr, _))| (id.clone(), addr.clone()))
            .collect::<Vec<_>>()
    }
    #[inline]
    pub fn connected_ids(&self) -> Vec<ID> {
        let map = self.map();
        map.senders
            .iter()
            .map(|(id, _)| id.clone())
            .collect::<Vec<_>>()
    }
    #[inline]
    pub fn connected_addrs(&self) -> Vec<SocketAddr> {
        let map = self.map();
        map.senders
            .iter()
            .map(|(_, &(ref addr, _))| addr.clone())
            .collect::<Vec<_>>()
    }

    #[inline]
    pub fn connected_count(&self) -> u64 {
        let map = self.map();
        map.senders.len() as u64
    }

    #[inline]
    pub fn send_bytes(&self, id: &ID, bytes: Vec<u8>) -> Result<(), Vec<u8>> {
        let map = self.map();

        if let Some(&(_, ref sender)) = map.senders.get(id) {
            sender.unbounded_send(bytes).map_err(|e| e.into_inner())
        } else {
            Err(bytes)
        }
    }
    #[inline]
    pub fn broadcast_bytes(&self, bytes: Vec<u8>) -> Result<(), (Vec<u8>, Vec<ID>)> {
        let map = self.map();
        let mut errors = Vec::new();

        for (id, &(_, ref sender)) in map.senders.iter() {
            if sender.unbounded_send(bytes.clone()).is_err() {
                errors.push(id.clone());
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err((bytes, errors))
        }
    }
    #[inline]
    pub fn broadcast_bytes_to<'a, I>(
        &self,
        ids: I,
        bytes: Vec<u8>,
    ) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        ID: Id,
        I: 'a + IntoIterator<Item = &'a ID>,
    {
        let map = self.map();
        let mut errors = Vec::new();

        for id in ids.into_iter() {
            let result = if let Some(&(_, ref sender)) = map.senders.get(id) {
                sender.unbounded_send(bytes.clone())
            } else {
                Ok(())
            };

            if result.is_err() {
                errors.push(id.clone());
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err((bytes, errors))
        }
    }
    #[inline]
    pub fn broadcast_bytes_except(
        &self,
        ids: &HashSet<ID>,
        bytes: Vec<u8>,
    ) -> Result<(), (Vec<u8>, Vec<ID>)> {
        let map = self.map();
        let mut errors = Vec::new();

        for (id, &(_, ref sender)) in map.senders.iter() {
            if !ids.contains(id) {
                if sender.unbounded_send(bytes.clone()).is_err() {
                    errors.push(id.clone());
                }
            }
        }

        if errors.is_empty() {
            Ok(())
        } else {
            Err((bytes, errors))
        }
    }
    #[inline]
    pub fn send_bytes_random(&self, bytes: Vec<u8>) -> Result<(), Vec<u8>> {
        let map = self.map();
        let mut peers = map.iter();
        let len = peers.len();

        if len > 0 {
            peers
                .nth(thread_rng().gen_range(0, len))
                .map(|(id, _)| self.send_bytes(id, bytes))
                .unwrap_or(Ok(()))
        } else {
            Ok(())
        }
    }

    #[inline]
    pub fn send<T>(&self, id: &ID, data: &T) -> Result<(), Vec<u8>>
    where
        T: Serialize,
    {
        match bincode::serialize(data, bincode::Infinite) {
            Ok(bytes) => self.send_bytes(id, bytes),
            Err(_) => Err(Vec::new()),
        }
    }
    #[inline]
    pub fn broadcast<T>(&self, data: &T) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        T: Serialize,
    {
        match bincode::serialize(data, bincode::Infinite) {
            Ok(bytes) => self.broadcast_bytes(bytes),
            Err(_) => Err((Vec::new(), self.connected_ids())),
        }
    }
    #[inline]
    pub fn broadcast_to<'a, I, T>(&self, ids: I, data: &T) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        ID: Id,
        I: 'a + IntoIterator<Item = &'a ID>,
        T: Serialize,
    {
        match bincode::serialize(data, bincode::Infinite) {
            Ok(bytes) => self.broadcast_bytes_to(ids, bytes),
            Err(_) => Err((Vec::new(), self.connected_ids())),
        }
    }
    #[inline]
    pub fn broadcast_except<T>(&self, ids: &HashSet<ID>, data: &T) -> Result<(), (Vec<u8>, Vec<ID>)>
    where
        T: Serialize,
    {
        match bincode::serialize(data, bincode::Infinite) {
            Ok(bytes) => self.broadcast_bytes_except(ids, bytes),
            Err(_) => Err((Vec::new(), self.connected_ids())),
        }
    }
    #[inline]
    pub fn send_random<T>(&self, data: &T) -> Result<(), Vec<u8>>
    where
        T: Serialize,
    {
        match bincode::serialize(data, bincode::Infinite) {
            Ok(bytes) => self.send_bytes_random(bytes),
            Err(_) => Err(Vec::new()),
        }
    }
}

pub struct PeersMap<ID>
where
    ID: Id,
{
    senders: HashMap<ID, (SocketAddr, UnboundedSender<Vec<u8>>)>,
    local_addrs: HashMap<SocketAddr, ID>,
}

impl<ID> PeersMap<ID>
where
    ID: Id,
{
    #[inline]
    pub fn new() -> Self {
        PeersMap {
            senders: HashMap::new(),
            local_addrs: HashMap::new(),
        }
    }

    #[inline(always)]
    pub fn iter<'a>(&'a self) -> hash_map::Iter<'a, ID, (SocketAddr, UnboundedSender<Vec<u8>>)> {
        self.into_iter()
    }
}

impl<'a, ID> IntoIterator for &'a PeersMap<ID>
where
    ID: Id,
{
    type Item = (&'a ID, &'a (SocketAddr, UnboundedSender<Vec<u8>>));
    type IntoIter = hash_map::Iter<'a, ID, (SocketAddr, UnboundedSender<Vec<u8>>)>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.senders.iter()
    }
}
