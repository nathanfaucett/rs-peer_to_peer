use serde::ser::Serialize;
use serde::de::DeserializeOwned;

pub trait Value: 'static + Send + Serialize + DeserializeOwned {}

impl<T> Value for T
where
    T: 'static + Send + Serialize + DeserializeOwned,
{
}
