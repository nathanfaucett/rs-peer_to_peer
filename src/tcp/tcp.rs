use std::convert::TryInto;
use std::io::{self, Error, ErrorKind};
use std::net::SocketAddr;

use futures::sync::mpsc::{unbounded, UnboundedSender};
use futures::sync::oneshot::Receiver;
use futures::{future, Future, Sink, Stream};
use tokio_io::AsyncRead;
use tokio::executor::current_thread;
use tokio::net::{TcpListener, TcpStream};

use super::super::{Event, Id, Packet, PacketCodec, Peer, Value};
use super::TcpClose;

/// # Panics
///
/// This function can only be invoked from the context of a `run` call; any
/// other use will result in a panic.
#[inline]
pub fn spawn<I, ID, T>(peer: &Peer<ID, T>, addrs: I) -> io::Result<TcpClose>
where
    I: IntoIterator<Item = SocketAddr>,
    ID: Id,
    T: Value,
{
    let (close, close_receiver) = TcpClose::new();

    spawn_server(peer, close_receiver)?;

    for addr in addrs {
        spawn_client(addr, peer)?;
    }

    Ok(close)
}

#[inline]
pub fn tcp<I, ID, T>(
    peer: &Peer<ID, T>,
    addrs: I,
) -> io::Result<(TcpClose, impl Future<Item = (), Error = ()> + Send)>
where
    I: IntoIterator<Item = SocketAddr>,
    ID: Id,
    T: Value,
{
    let (close, close_receiver) = TcpClose::new();

    let mut futures: Vec<Box<Future<Item = (), Error = ()> + Send>> = Vec::new();

    let server_future = server(peer, close_receiver)?;

    futures.push(Box::new(server_future));

    for addr in addrs {
        let client_future = client(addr, peer)?;
        futures.push(Box::new(client_future));
    }

    let f = future::join_all(futures).map(|_| ()).map_err(|_| ());

    Ok((close, f))
}

#[inline]
fn server<ID, T>(
    peer: &Peer<ID, T>,
    close_receiver: Receiver<()>,
) -> io::Result<impl Future<Item = (), Error = ()> + Send>
where
    ID: Id,
    T: Value,
{
    let peer = peer.clone();
    let tcp_listener = TcpListener::bind(peer.addr())?;

    let error_peer = peer.clone();
    let event_peer = peer.clone();

    let f = tcp_listener
        .incoming()
        .for_each(move |socket| {
            let local_addr = socket.local_addr().unwrap();

            let (sink, stream) = socket.framed(PacketCodec::new()).split();
            let (sender, receiver) = unbounded();

            let read_peer = peer.clone();
            let read_sender = sender;
            let read_local_addr = local_addr.clone();

            let error_peer = peer.clone();

            let close_peer = peer.clone();
            let close_local_addr = local_addr.clone();

            let read = stream
                .for_each(move |packet| {
                    process_packet(&read_peer, packet, read_local_addr.clone(), &read_sender)
                })
                .map_err(move |e| error_peer.send_event(Event::Error(e.into())))
                .then(move |_| {
                    close_peer.disconnect(&close_local_addr);
                    Ok(())
                });

            current_thread::spawn(read);

            let error_peer = peer.clone();

            let close_peer = peer.clone();
            let close_local_addr = local_addr;

            let write = sink.send_all(
                receiver
                    .map(|packet| packet.try_into().unwrap())
                    .map_err(|_| Error::new(ErrorKind::Other, "receiver should not have an error")),
            ).map_err(move |e| error_peer.send_event(Event::Error(e.into())))
                .then(move |_| {
                    close_peer.disconnect(&close_local_addr);
                    Ok(())
                });

            current_thread::spawn(write);

            Ok(())
        })
        .map_err(move |e| error_peer.send_event(Event::Error(e.into())))
        .select(close_receiver.map(|_| ()).map_err(|_| ()))
        .map(|_| ())
        .map_err(|_| ());

    event_peer.send_event(Event::Listen(
        event_peer.id().clone(),
        event_peer.addr().clone(),
    ));

    Ok(f)
}

#[inline]
fn spawn_server<ID, T>(peer: &Peer<ID, T>, close_receiver: Receiver<()>) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    let server_future = server(peer, close_receiver)?;
    current_thread::spawn(server_future);
    Ok(())
}

#[inline]
fn client<ID, T>(
    client_addr: SocketAddr,
    peer: &Peer<ID, T>,
) -> io::Result<impl Future<Item = (), Error = ()> + Send>
where
    ID: Id,
    T: Value,
{
    let peer = peer.clone();
    let server_id = peer.id().clone();
    let server_addr = peer.addr().clone();
    let client_addr = client_addr.clone();

    let error_peer = peer.clone();

    let f = TcpStream::connect(&client_addr)
        .and_then(move |socket| {
            let local_addr = socket.local_addr().unwrap();

            let (sink, stream) = socket.framed(PacketCodec::new()).split();
            let (sender, receiver) = unbounded();

            let read_peer = peer.clone();
            let read_sender = sender.clone();
            let read_local_addr = local_addr.clone();

            let error_peer = peer.clone();

            let close_peer = peer.clone();
            let close_local_addr = local_addr.clone();

            let read = stream
                .for_each(move |packet| {
                    process_packet(&read_peer, packet, read_local_addr.clone(), &read_sender)
                })
                .map_err(move |e| error_peer.send_event(Event::Error(e.into())))
                .then(move |_| {
                    close_peer.disconnect(&close_local_addr);
                    Ok(())
                });

            current_thread::spawn(read);

            let packet = Packet::Ping::<ID, T>(server_id, server_addr)
                .try_into()
                .unwrap();
            let _ = sender.unbounded_send(packet).expect("failed to send ping");

            let error_peer = peer.clone();

            let close_peer = peer;
            let close_local_addr = local_addr;

            let write = sink.send_all(
                receiver
                    .map(|packet| packet.try_into().unwrap())
                    .map_err(|_| Error::new(ErrorKind::Other, "receiver should not have an error")),
            ).map_err(move |e| error_peer.send_event(Event::Error(e.into())))
                .then(move |_| {
                    close_peer.disconnect(&close_local_addr);
                    Ok(())
                });

            current_thread::spawn(write);

            Ok(())
        })
        .map_err(move |e| error_peer.send_event(Event::ConnectError(e.into())));

    Ok(f)
}

#[inline]
fn spawn_client<ID, T>(client_addr: SocketAddr, peer: &Peer<ID, T>) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    let client_future = client(client_addr, peer)?;
    current_thread::spawn(client_future);
    Ok(())
}

#[inline]
fn process_packet<ID, T>(
    peer: &Peer<ID, T>,
    packet: Packet<ID, T>,
    local_addr: SocketAddr,
    sender: &UnboundedSender<Vec<u8>>,
) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    match packet {
        Packet::Ping(ping_id, ping_addr) => {
            process_ping(peer, sender, ping_id, ping_addr, local_addr)
        }
        Packet::Pong(pong_id, pong_addr) => {
            process_pong(peer, sender, pong_id, pong_addr, local_addr)
        }
        Packet::Payload(from_id, data) => process_payload(peer, from_id, data),
        Packet::Bytes(from_id, bytes) => process_bytes(peer, from_id, bytes),
        Packet::Peers(peers) => {
            for (id, addr) in peers {
                if !peer.is_id_connected(&id) {
                    spawn_client(addr, peer)?;
                }
            }
            Ok(())
        }
    }
}

#[inline]
fn process_ping<ID, T>(
    peer: &Peer<ID, T>,
    sender: &UnboundedSender<Vec<u8>>,
    ping_id: ID,
    ping_addr: SocketAddr,
    local_addr: SocketAddr,
) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    if !peer.is_id_connected(&ping_id) {
        let packet = Packet::Pong::<ID, T>(peer.id().clone(), peer.addr().clone())
            .try_into()
            .unwrap();
        let _ = sender.unbounded_send(packet).expect("failed to send ping");

        peer.connect(ping_id, ping_addr, local_addr, sender.clone());
    }
    Ok(())
}

#[inline]
fn process_pong<ID, T>(
    peer: &Peer<ID, T>,
    sender: &UnboundedSender<Vec<u8>>,
    pong_id: ID,
    pong_addr: SocketAddr,
    local_addr: SocketAddr,
) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    if !peer.is_id_connected(&pong_id) {
        peer.connect(pong_id, pong_addr, local_addr, sender.clone());
    }
    Ok(())
}

#[inline]
fn process_payload<ID, T>(peer: &Peer<ID, T>, from_id: ID, data: T) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    peer.send_event(Event::Payload(from_id, data));
    Ok(())
}

#[inline]
fn process_bytes<ID, T>(peer: &Peer<ID, T>, from_id: ID, bytes: Vec<u8>) -> io::Result<()>
where
    ID: Id,
    T: Value,
{
    peer.send_event(Event::Bytes(from_id, bytes));
    Ok(())
}
