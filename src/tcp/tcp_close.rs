use futures::sync::oneshot::{channel, Receiver, Sender};

pub struct TcpClose(Sender<()>);

impl TcpClose {
    #[inline]
    pub(crate) fn new() -> (TcpClose, Receiver<()>) {
        let (sender, receiver) = channel();
        (TcpClose(sender), receiver)
    }

    #[inline]
    pub fn close(self) {
        let _ = self.0.send(());
    }
}
