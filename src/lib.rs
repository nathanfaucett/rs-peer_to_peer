#![feature(try_from)]
#![feature(conservative_impl_trait)]

extern crate bincode;
extern crate bytes;

extern crate futures;

extern crate serde;
#[macro_use]
extern crate serde_derive;

extern crate tokio;
extern crate tokio_io;

extern crate rand;

mod common;
pub mod tcp;

pub use self::common::{Event, Id, Packet, PacketCodec, Peer, Peers, Value};
